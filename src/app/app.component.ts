import { Component } from '@angular/core';
import { UserApiService } from './infraestructure/driven-adapters/user-api/user-api.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'my-music-app';

  constructor(private userService:UserApiService){
     this.userService.initAuthListener();
  }
  
}
