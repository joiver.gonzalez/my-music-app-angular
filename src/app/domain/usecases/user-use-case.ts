import { Injectable } from "@angular/core";
import { UserGateway } from '../models/User/gateway/user-gateway';
import { UserCredential } from "../models/User/interfaces/user.auth.interfaces";
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})
export class UserUsecase{
    constructor(private _userGateway: UserGateway){}

    loginUser(email:string,password:string):Observable<UserCredential>{
        return this._userGateway.loginUser(email,password);
    }

    registerUser(email:string,password:string):Observable<UserCredential>{
       return this._userGateway.registerUser(email,password);
    }

    logout():Observable<void>{
       return this._userGateway.logout();
    }

    isAuth():Observable<boolean>{
       return this._userGateway.isAuth();
    }

}