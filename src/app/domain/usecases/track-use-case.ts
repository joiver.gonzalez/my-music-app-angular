import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TrackGateway } from "../models/Track/gateway/track-gateway";
import { Track } from "../models/Track/track";

@Injectable({
    providedIn:'root'
})
export class TrackUseCase{
    constructor(private _trackGateway:TrackGateway){}

    save(track:Track):Observable<void>{
     return this._trackGateway.save(track);
    }
    getByUserId():Observable<Array<Track>>{
       return this._trackGateway.getByUserId();
    }
    deleteById(id:string):Observable<void>{
      return this._trackGateway.deleteById(id);
    }
}