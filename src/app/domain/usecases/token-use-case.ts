import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Token } from "../models/Token/token";
import { TokenGateway } from "../models/Token/gateway/token-gateway";

@Injectable({
    providedIn:'root'
})
export class TokenUseCase{
    constructor(private _tokenGateway:TokenGateway){}

    getToken():Promise<Token>{
        return this._tokenGateway.getToken();
    }
}