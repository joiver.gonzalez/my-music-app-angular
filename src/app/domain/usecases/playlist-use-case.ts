import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Playlist } from "../models/Playlist/playlist";
import { PlaylistGateway } from "../models/Playlist/gateway/playlist-gateway";

@Injectable({
    providedIn:'root'
})
export class PlaylistUseCases{
    constructor(private _playlistGateway:PlaylistGateway){}
    getPlaylist(token:string):Observable<Playlist>{
        return this._playlistGateway.getPlaylist(token);
    }
}