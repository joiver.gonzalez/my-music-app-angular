import { Observable } from "rxjs";
import { Track } from "../track";

export abstract class TrackGateway {
     abstract save(track: Track):Observable<void>;
    abstract getByUserId():Observable<Array<Track>>;
    abstract deleteById(id:string):Observable<void>;
}