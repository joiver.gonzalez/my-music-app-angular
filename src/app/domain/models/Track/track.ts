
export class Track{
    constructor(
        public id:string,
        public name:string,
        public artistName:string|undefined,
        public urlImage:string,
        public isFavorite:boolean
        ){}
}