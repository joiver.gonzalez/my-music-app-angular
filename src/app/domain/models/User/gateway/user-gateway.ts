import { UserCredential, Logout } from '../interfaces/user.auth.interfaces';
import { User } from "../user";
import { Observable } from 'rxjs';

export abstract class UserGateway {
    
    abstract loginUser(email:string,password:string): Observable<UserCredential>;
    abstract registerUser(email:string,password:string): Observable<UserCredential>;
    abstract logout():Observable<void>;
    abstract isAuth():Observable<boolean>
}