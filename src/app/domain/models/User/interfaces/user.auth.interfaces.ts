// export interface User {
//     __zone_symbol__state: boolean;
//     __zone_symbol__value: ZoneSymbolValue;
// }
export interface Logout {
    __zone_symbol__state: boolean;
}

export interface UserCredential {
    operationType?:      string | null | undefined;
    credential:         AuthCredential | null;
    additionalUserInfo?: AdditionalUserInfo | null | undefined;
    user:               User | null;
}

export interface AdditionalUserInfo {
    isNewUser:  boolean;
    providerId: string;
    profile:    Object | null;
}

export interface Profile {
    
}

export interface User {
    uid:             string;
    email:           string | null;
    emailVerified:   boolean;
    isAnonymous:     boolean;
    providerData:    (UserInfo |null)[] ;
   
}

export interface UserInfo {
    providerId:  string;
    uid:         string;
    displayName: string | null;
    email:       string | null;
    phoneNumber: string | null;
    photoURL:   string | null;
}

export interface AuthCredential{
    providerId:String;
    signInMethod:string,
    toJSON():Object
}

export interface StsTokenManager {
    refreshToken:   string;
    accessToken:    string;
    expirationTime: number;
}