import { Observable } from "rxjs";
import { Playlist } from "../playlist";

export abstract class PlaylistGateway{
    abstract getPlaylist(token:string):Observable<Playlist>;
}