import { Item } from "./interfaces/playlist.interface";

export class Playlist{
    
    constructor(
    public href:     string,
    public items:    Item[],
    public limit:    number,
    public next:     null,
    public offset:   number,
    public previous: null,
    public total:    number,
    ){}

    
}