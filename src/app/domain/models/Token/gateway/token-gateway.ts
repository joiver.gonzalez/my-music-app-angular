import { Observable } from "rxjs";
import { Token } from "../token";

export abstract class TokenGateway{
    abstract getToken():Promise<Token>;
}