import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../environments/environment.prod';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { StoreModule } from '@ngrx/store';
import { appReducers } from './infraestructure/store/app.reducer';
import { UiModule } from './ui/ui.module';
import { AppRouterModule } from './app-router.module';
describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports:[
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        StoreModule.forRoot(appReducers),
        UiModule,
        AppRouterModule
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
  it('should have as title my-music-app',()=>{
    const fixture = TestBed.createComponent(AppComponent);
    const app=fixture.componentInstance;
    expect(app.title).toEqual('my-music-app');
  })

});
