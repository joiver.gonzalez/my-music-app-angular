import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './infraestructure/guards/auth.guard';



const routes: Routes=[
  {
    path: 'spotify',
    loadChildren:()=>import('./ui/spotify/spotify.module').then(m=>m.SpotifyModule),
    canActivate:[AuthGuard]
  },
 
  {
    path: 'auth',
    loadChildren:()=>import('./ui/auth/auth.module').then(m=>m.AuthModule)
  },
  {
    path:'**',
    redirectTo:'spotify/home'
  }
  
]

@NgModule({
  declarations: [],
  exports:[
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRouterModule { }
