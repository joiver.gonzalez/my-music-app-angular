import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/infraestructure/store/app.state';
import { Track } from 'src/app/domain/models/Track/track';
import { setFavorites, deleteFavorite } from '../../../infraestructure/store/favorites/favorites.actions';
import { selectFavorites, selectUi } from 'src/app/infraestructure/store/selectors';
import { updatePlaylist } from '../../../infraestructure/store/playlist/playlist.actions';


@Component({
  selector: 'app-favorites',
  changeDetection:ChangeDetectionStrategy.OnPush,
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit,OnDestroy{

  hasTracks:boolean=false;
  loading:boolean=false;

  favoritesObs!:Observable<Track[]>;
  uiSubs!:Subscription;
  favorites:Track[]=[];
 

  constructor(
    private store:Store<AppState>
    ) { }

    

  ngOnInit(): void {
    this.uiSubs=this.store.select(selectUi).subscribe(ui=>{
      this.loading=ui;
      console.log('Cargando...',this.loading);
    })   

    this.store.dispatch(setFavorites())
    
     this.favoritesObs=this.store.select(selectFavorites);  
 
  }
 
  ngOnDestroy(): void { 
    this.uiSubs.unsubscribe();
  }

 
  clickFunction(item:Track){
    this.store.dispatch(deleteFavorite({id:item.id}))
    this.store.dispatch(updatePlaylist({isFavorite:false,id:item.id}))
 }


}
