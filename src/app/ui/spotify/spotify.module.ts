import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { SpotifyRoutingModule } from './spotify-routing.module';
import { AtomicDesignModule } from '../components/atomic-design.module';
import { SharedModule } from '../../infraestructure/shared/shared.module';




@NgModule({
  declarations: [
    HomeComponent,
    FavoritesComponent
  ],
  exports:[
    HomeComponent,
    FavoritesComponent
  ],
  imports: [
    CommonModule,
    SpotifyRoutingModule,
    AtomicDesignModule,
    SharedModule,
  ]
})
export class SpotifyModule { }
