import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FavoritesComponent } from './favorites/favorites.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {
        path: 'home',
      component: HomeComponent
      },
      {
        path:'my-favorites',
        component: FavoritesComponent
      },
      {
        path:'**',
        redirectTo: 'home'
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpotifyRoutingModule { }
