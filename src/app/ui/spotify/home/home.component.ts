import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  Subscription,
  filter,
  finalize,
  debounceTime,
  lastValueFrom,
} from 'rxjs';
import {
  setPlaylist,
  updatePlaylistSuccess,
} from 'src/app/infraestructure/store/playlist/playlist.actions';
import { fade } from 'src/app/infraestructure/shared/animations';
import { Track } from 'src/app/domain/models/Track/track';
import { AppState } from 'src/app/infraestructure/store/app.state';
import { Token } from 'src/app/domain/models/Token/token';
import { selectUi } from '../../../infraestructure/store/ui/ui.selector';
import {
  updateFavorites,
  setFavorites,
} from '../../../infraestructure/store/favorites/favorites.actions';
import {
  selectFilteredPlaylist,
  selectPlaylist,
} from '../../../infraestructure/store/playlist/playlist.selector';
import { isLoading } from 'src/app/infraestructure/store/ui/ui.actions';
import { stopLoading } from '../../../infraestructure/store/ui/ui.actions';
import { updatePlaylist } from '../../../infraestructure/store/playlist/playlist.actions';
import { setToken, setTokenSuccess } from '../../../infraestructure/store/token/token.actions';
import { selectToken } from '../../../infraestructure/store/token/token.selectors';
import { selectFavorites } from '../../../infraestructure/store/selectors/index';
import { TokenApiService } from '../../../infraestructure/driven-adapters/token-api/token-api.service';
import { updateFavoritesSuccess } from '../../../infraestructure/store/favorites/favorites.actions';
import { selectFilteredFavorites } from 'src/app/infraestructure/store/favorites/favorites.selectors';
import { detecFavoritesTracksAndReturn } from 'src/app/infraestructure/helpers/detectFavoritesTracks';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fade],
})
export class HomeComponent implements OnInit, OnDestroy {
  playlistSubscription!: Subscription;
  favoritesSubscription!: Subscription;
  playlist: Track[] = [];
  playlistNoFavorites: Track[] = [];
  favorites: Track[] = [];
  token:string='';
  constructor(
    private store: Store<AppState>,
    private tokenApiService: TokenApiService
  ) {}

  ngOnInit(): void {
    this.token=localStorage.getItem("token")||"";
    if(!this.token){
      this.tokenApiService.getToken().then((token) => {
      localStorage.setItem("token",token.access_token);
      this.store.dispatch(setPlaylist({ token: token.access_token}));
      }).catch((error)=>console.log(error));
    }else{
      this.store.dispatch(setPlaylist({ token: this.token}));
    }
    
    
    
    this.store.dispatch(setFavorites());
    this.loadFavorites();
    this.loadPlaylist();
  }

  ngOnDestroy(): void {
    this.playlistSubscription.unsubscribe();
    this.favoritesSubscription.unsubscribe();
  }

  loadFavorites() {
    this.favoritesSubscription = this.store
      .pipe(selectFilteredFavorites)
      .subscribe((favs) => {
        this.favorites = favs;
        const newListOfTracksWhitFavorites:Track[]=detecFavoritesTracksAndReturn(this.playlistNoFavorites,this.favorites);
        this.playlist = newListOfTracksWhitFavorites;
        //this.store.dispatch(updatePlaylistSuccess({playlist:tracksWhitFavorites}))
      });
  }

  loadPlaylist() {
    this.playlistSubscription = this.store
      .pipe(selectFilteredPlaylist)
      .subscribe({
        next: (tracks) => {
          this.playlistNoFavorites = tracks;
        },
        error: (err) => console.log(err),
      });
  }


  clickFunction(item: Track) {
    this.store.dispatch(updateFavorites({ newFavorite: item }));
  }
}
