import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from './auth/auth.module';
import { SpotifyModule } from './spotify/spotify.module';
import { AtomicDesignModule } from './components/atomic-design.module';




@NgModule({
  declarations: [],
  exports:[
    AtomicDesignModule,
    AuthModule,
    SpotifyModule
  ],
  imports: [
    CommonModule,
    AuthModule,
    SpotifyModule,
    AtomicDesignModule,
  ]
})
export class UiModule { }
