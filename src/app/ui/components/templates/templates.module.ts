import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { OrganismModule } from '../organism/organism.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LayoutComponent
  ],
  exports:[
    LayoutComponent
  ],
  imports: [
    CommonModule,
    OrganismModule
  ]
})
export class TemplatesModule { }
