import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @Input() className: string='';
  @Input() type: string='';
  @Input() placeholder:string='';
  @Input() set control(value: FormControl){
    if(this.formControl!==value){
      this.formControl=value;
    }
  }
  formControl!: FormControl;
  constructor() { }

  ngOnInit(): void {
  }

}
