import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  @Input() className:string='';
  @Input() srcImage:string='';
  @Input() altImage:string='';

  constructor() { }

  ngOnInit(): void {
  }

}
