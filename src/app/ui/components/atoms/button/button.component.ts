import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() className: string='';
  @Input() buttonType: string='';
  @Input() isDisable?: boolean=false;
 
  
  constructor(private router:Router){

  }
  
  

}
