import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './../atoms/button/button.component';
import { LinkComponent } from './link/link.component';
import { ImageComponent } from './image/image.component';
import { TitleComponent } from './title/title.component';
import { LabelComponent } from './label/label.component';
import { InputComponent } from './input/input.component';
import { RouterModule } from '@angular/router';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonClickComponent } from './button-click/button-click.component';


@NgModule({
  declarations: [
    ButtonComponent,
    LinkComponent,
    ImageComponent,
    TitleComponent,
    LabelComponent,
    InputComponent,
    ParagraphComponent,
    ButtonClickComponent
  ],
  exports:[
    ButtonComponent,
    LinkComponent,
    ImageComponent,
    TitleComponent,
    LabelComponent,
    InputComponent,
    ParagraphComponent,
    ButtonClickComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class AtomsModule { }
