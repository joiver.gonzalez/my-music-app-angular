import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/infraestructure/store/app.state';

@Component({
  selector: 'app-button-click',
  templateUrl: './button-click.component.html',
  styleUrls: ['./button-click.component.scss'],
})
export class ButtonClickComponent implements OnInit {
  
  @Input() className: string='';
  @Input() buttonType: string='';
  @Input() item:any;
  @Input() clickFunction!:(item:any)=>void;

  isLiked:boolean=false;
 
  constructor(
    private router:Router,
    private store:Store<AppState>
     ) { }

  ngOnInit(): void {
   
  }
  
 
 

}
