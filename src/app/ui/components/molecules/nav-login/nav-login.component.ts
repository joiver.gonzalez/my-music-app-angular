import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from '../../../services/auth.service';
import { AppState } from 'src/app/infraestructure/store/app.state';
import { Store } from '@ngrx/store';
import { unSetUser } from '../../../../infraestructure/store/user/user.actions';

@Component({
  selector: 'app-nav-login',
  templateUrl: './nav-login.component.html',
  styleUrls: ['./nav-login.component.scss']
})
export class NavLoginComponent implements OnInit {

 
  @Input() userEmail:string='';

  constructor(private router:Router, 
    private store:Store<AppState>
    ) { }

  ngOnInit(): void {
  }
  clickFunction(){
    this.store.dispatch(unSetUser())
    this.router.navigate(['auth/login'])
    // console.log('Cerrar sesión')
    // this.authService.logout().then(()=>{
    //   this.router.navigate(['auth/login'])
    // });
    
  }

}
