import { Component, Input } from '@angular/core';
import { Track } from 'src/app/domain/models/Track/track';


@Component({
  selector: 'app-info-input',
  templateUrl: './info-input.component.html',
  styleUrls: ['./info-input.component.scss']
})
export class InfoInputComponent {
  @Input() trackName:string='';
  @Input() artistName:string|undefined='';
  @Input() item!:Track;
  @Input() isLiked!:boolean;
  @Input() clickFunction!:(item:Track)=>void;
  
  srcImageHeart:string='./../../assets/icons/icons8-heart-35.png';
  srcImageHeartFull:string='./../../assets/icons/icons8-heart-35-full.png';
  

  constructor() { }

  get srcButton(){
    return this.item.isFavorite?this.srcImageHeartFull:this.srcImageHeart;
  }
}
