import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-unordered-list',
  templateUrl: './unordered-list.component.html',
  styleUrls: ['./unordered-list.component.scss']
})
export class UnorderedListComponent implements OnInit {
  @Input() className: string='';

  constructor() { }

  ngOnInit(): void {
  }

}
