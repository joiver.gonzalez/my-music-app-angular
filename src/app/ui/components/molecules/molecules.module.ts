import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackInfoComponent } from './track-info/track-info.component';
import { AtomsModule } from '../atoms/atoms.module';
import { InfoInputComponent } from './info-input/info-input.component';
import { TrackCardComponent } from './track-card/track-card.component';
import { UnorderedListComponent } from './unordered-list/unordered-list.component';
import { NavNotLoginComponent } from './nav-not-login/nav-not-login.component';
import { NavLoginComponent } from './nav-login/nav-login.component';




@NgModule({
  declarations: [TrackInfoComponent, InfoInputComponent, TrackCardComponent, UnorderedListComponent, NavNotLoginComponent, NavLoginComponent],
  exports:[
    TrackInfoComponent,
    InfoInputComponent, 
    TrackCardComponent,
    UnorderedListComponent,
    NavNotLoginComponent,
    NavLoginComponent
  ],
  imports: [
    CommonModule,
    AtomsModule
  ]
})
export class MoleculesModule { }
