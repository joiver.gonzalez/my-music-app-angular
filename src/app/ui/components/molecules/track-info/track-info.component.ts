import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-track-info',
  templateUrl: './track-info.component.html',
  styleUrls: ['./track-info.component.scss']
})
export class TrackInfoComponent implements OnInit {

  @Input() trackName:string='';
  @Input() artistName:string|undefined='';
  constructor() { }

  ngOnInit(): void {
  }

}
