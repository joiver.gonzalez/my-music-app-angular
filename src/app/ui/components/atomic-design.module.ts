import { NgModule } from '@angular/core';
import { AtomsModule } from './atoms/atoms.module';
import { MoleculesModule } from './molecules/molecules.module';
import { OrganismModule } from './organism/organism.module';
import { TemplatesModule } from './templates/templates.module';




@NgModule({
  declarations: [
  ],
  exports:[
    AtomsModule,
     OrganismModule,
     MoleculesModule,
     TemplatesModule,     
  ],
  
})
export class AtomicDesignModule { }
