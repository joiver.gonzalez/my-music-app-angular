import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MoleculesModule } from '../molecules/molecules.module';
import { AtomsModule } from '../atoms/atoms.module';
import { InfraestructureModule } from 'src/app/infraestructure/infraestructure.module';




@NgModule({
  declarations: [HeaderComponent],
  exports:[
    HeaderComponent,

  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    InfraestructureModule
  ]
})
export class OrganismModule { }
