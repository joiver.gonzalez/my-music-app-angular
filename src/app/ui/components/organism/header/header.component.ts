import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/infraestructure/store/app.state';
import { User } from 'src/app/domain/models/User/user';
import { selectUser } from '../../../../infraestructure/store/selectors/index';
import { map, filter } from 'rxjs';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  userState:User | null=null;

  constructor(private store:Store<AppState>) {
   }

  ngOnInit(): void {
     this.store.select(selectUser).subscribe(user=>{
      this.userState=user
     })
     
  }

  get userEmail():string{
      return this.userState?.email?this.userState.email:''; 
  }



}
