import {  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AtomicDesignModule } from '../components/atomic-design.module';
import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';





@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
  ],
  exports:[
   LoginComponent,
   RegisterComponent
  ],
  imports: [
    CommonModule,
    AtomicDesignModule,
    AuthRoutingModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
