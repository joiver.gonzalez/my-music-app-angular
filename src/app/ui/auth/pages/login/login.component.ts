import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ValidatorService } from 'src/app/infraestructure/helpers/validator/validator.service';
import { Router } from '@angular/router';
import { AppState } from 'src/app/infraestructure/store/app.state';
import * as ui from '../../../../infraestructure/store/ui/ui.actions';
import { Subscription, filter } from 'rxjs';
import { setUser } from 'src/app/infraestructure/store/user/user.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy{

  loading:boolean=false;
  uiSubscription!: Subscription;
  userSubscription!:Subscription;

  miFormulario: FormGroup=this.fb.group({
    email:['',[Validators.required]],
    password:['',[Validators.required, Validators.minLength(6)]]
  });


  constructor(
    private fb: FormBuilder,
     private vs:ValidatorService,
      private router:Router,
      private store: Store<AppState>
      ) {
   }

  ngOnInit(): void {
    this.uiSubscription=this.store.select('ui').subscribe(ui=>{
      this.loading=ui.isLoading;
      console.log('Cargando...',this.loading);
    },
    (error)=>console.log(error))

   this.userSubscription=this.store.select('user').pipe(
      filter(auth=>auth.user!=null)
    ).subscribe(({user})=>{
         if(user){
          this.store.dispatch(ui.stopLoading()); 
          this.router.navigate(['spotify/home']);
         }
    },
    (error)=>console.log(error)) 
  }
  
  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
  
  campoNoValido(campo: string){
    return this.miFormulario.get(campo)?.invalid && this.miFormulario.get(campo)?.touched
  }
  get emailErrorMsg():string{
    const errors=this.miFormulario.get('email')?.errors;
    if(errors?.['required']){
      return 'Email es obligatorio'
    }else if(errors?.['pattern']){
      return 'El valor ingresado no tiene formato de correo'
    }
    return ''
  }

  submitFormulario(): void{
    this.store.dispatch(ui.isLoading())
    if(this.miFormulario.invalid){
      return;
    }
    const {email,password}=this.miFormulario.value;
    this.store.dispatch(setUser({email,password}));
    this.miFormulario.markAllAsTouched();
    
  }

}
