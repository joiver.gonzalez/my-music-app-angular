import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidatorService } from 'src/app/infraestructure/helpers/validator/validator.service';
//import { AuthService } from '../../../services/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/infraestructure/store/app.state';
import * as ui from '../../../../infraestructure/store/ui/ui.actions';
import { Subscription, filter } from 'rxjs';
import { registerUser } from 'src/app/infraestructure/store/user/user.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  loading: boolean=false;
  uiSubscription!:Subscription;
  userSubscription!:Subscription;

  miFormulario: FormGroup=this.fb.group({
    email:['',[Validators.required, Validators.pattern(this.vs.emailPattern)]],
    password:['',[Validators.required, Validators.minLength(6)]],
    password2:['',[Validators.required, Validators.minLength(6)]]
  },{
    validators: [this.vs.camposIguales('password','password2')]
  });


  constructor(
    private fb: FormBuilder,
    private vs:ValidatorService,
    // private authService:AuthService,
    private router:Router,
    private store:Store<AppState>
    ) { }

  ngOnInit(): void {
    this.uiSubscription=this.store.select('ui').subscribe(ui=>{
      this.loading=ui.isLoading;
      console.log('Cargando...')
    })
    this.userSubscription=this.store.select('user').pipe(
      filter(auth=>auth.user!=null)
    ).subscribe(({user})=>{
         if(user){
          this.store.dispatch(ui.stopLoading()); 
          this.router.navigate(['spotify/home']);
         }
    }) 
  }
  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }
  
  campoNoValido(campo: string){
    return this.miFormulario.get(campo)?.invalid && this.miFormulario.get(campo)?.touched
  }
  get emailErrorMsg():string{
    const errors=this.miFormulario.get('email')?.errors;
    if(errors?.['required']){
      return 'Email es obligatorio'
    }else if(errors?.['pattern']){
      return 'El valor ingresado no tiene formato de correo'
    }
    return ''
  }
  
  submitFormulario(){
    this.store.dispatch(ui.isLoading())
    if(this.miFormulario.invalid){
      return;
    }
     const {email,password}=this.miFormulario.value;
     this.store.dispatch(registerUser({email,password}));
     this.miFormulario.markAllAsTouched();
   }

}
