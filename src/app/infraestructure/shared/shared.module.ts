import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValueArrayPipe } from './pipes/value-array.pipe';



@NgModule({
  declarations: [ValueArrayPipe],
  imports: [
    CommonModule
  ],
  exports:[
    ValueArrayPipe
  ]
})
export class SharedModule { }
