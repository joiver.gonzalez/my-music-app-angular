import { Pipe, PipeTransform } from '@angular/core';
import { Track } from 'src/app/domain/models/Track/track';

@Pipe({
  name: 'valueArray'
})
export class ValueArrayPipe implements PipeTransform {

  transform(objects : Track[]) {
    return Object.values(objects);
  }

}
