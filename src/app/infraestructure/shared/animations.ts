import { trigger, state, style, transition, animate, animateChild } from '@angular/animations';


export const fade=trigger('fade',[
    state('void',style({opacity:0})),
    transition(':enter, :leave',[
      animate(500)
    ])
  ])

export const fadeX=trigger('fadeX',[
  state('void',style({opacity:0})),
  transition(':leave',[
    animate(500)
  ])
])  

