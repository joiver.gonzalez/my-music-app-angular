import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { TokenApiService } from '../../driven-adapters/token-api/token-api.service';
import { CoreTokenActionsUnion, TokenActions } from './token.actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
@Injectable()
export class TokenEffects{

    

    // loadToken$: Observable<Action>=createEffect(()=>this.actions$.
    //     mergeMap(
    //         ()=>this.tokenApiService.getToken()
    //     .pipe(
    //         map(token=>({
    //             type:TokenActions.setTokenSuccess,
    //             token:token
    //         }))
    //     )
    //     )
    // ))

    constructor(
        private actions$:Actions<CoreTokenActionsUnion>,
        private tokenApiService:TokenApiService,
    ){}
}