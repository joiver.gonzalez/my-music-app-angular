import { createAction, props, Action, union } from '@ngrx/store';
import { Token } from 'src/app/domain/models/Token/token';

export enum TokenActions{
    setToken='[Token] setToken',
    setTokenSuccess='[Token] setTokenSuccess',
    unSetToken='[Token] unSetToken'
}
export const setToken=createAction(TokenActions.setToken);
export const setTokenSuccess=createAction(TokenActions.setTokenSuccess,props<{token:string}>());
export const unSetToken=createAction(TokenActions.unSetToken);

const all = union({
    setToken,
    setTokenSuccess, 
    unSetToken
})

export type CoreTokenActionsUnion=typeof all;