import { createSelector } from "@ngrx/store";
import { AppState } from "../app.state";
import {State} from './token.state';


export const _selectToken=(state:AppState)=>state.token;

export const selectToken=createSelector(
    _selectToken,
    (state:State)=>state.token
);