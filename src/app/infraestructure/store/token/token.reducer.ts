import { Action, createReducer, on } from '@ngrx/store';
import { setToken, setTokenSuccess,unSetToken } from './token.actions';
import { initialState, State } from './token.state';

const _tokenReducer=createReducer(
    initialState,
    on(setToken,(state)=>({...state})),
    on(setTokenSuccess,(state,{token})=>({...state,token:token})),
    on(unSetToken,(state)=>({...state,token:""}))
    )
    export function tokenReducer(state: State  | undefined, action: Action) {
        return _tokenReducer(state, action);
    }
