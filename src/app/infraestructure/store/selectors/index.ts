import { createSelector } from "@ngrx/store";
import {AppState} from '../app.state';
import * as ui from '../ui/ui.state';
import * as user from '../user/user.state';
import * as playlist from '../playlist/playlist.state';
import * as favorites from '../favorites/favorites.state';


export const _selectUi=(state:AppState)=>state.ui;
export const _selectUser=(state:AppState)=>state.user;
export const _selectPlaylist=(state:AppState)=>state.playlist;
export const _selectFavorites=(state:AppState)=>state.favorites;

export const selectUi=createSelector(
    _selectUi,
    (state:ui.State)=>state.isLoading
);

export const selectUser=createSelector(
    _selectUser,
    (state:user.State)=>state.user
);

export const selectPlaylist=createSelector(
    _selectPlaylist,
    (state:playlist.State)=>state.playlist
)

export const selectFavorites=createSelector(
    _selectFavorites,
    (state:favorites.State)=>state.favorites
)


