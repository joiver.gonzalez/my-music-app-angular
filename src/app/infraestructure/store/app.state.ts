import * as user from './user/user.state';
import * as playlist from './playlist/playlist.state';
import * as favorites from './favorites/favorites.state';
import * as ui from './ui/ui.state';
import * as token from './token/token.state'

export interface AppState{
   user: user.State;
   playlist:playlist.State;
   favorites:favorites.State,
   ui:ui.State,
   token:token.State
}