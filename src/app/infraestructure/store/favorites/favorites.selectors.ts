import { createSelector, select } from '@ngrx/store';
import {AppState} from '../app.state';
import * as favorites from './favorites.state';
import { pipe, filter } from 'rxjs';

export const _selectFavorites=(state:AppState)=>state.favorites;

export const selectFavorites=createSelector(
    _selectFavorites,
    (state:favorites.State)=>state.favorites
);

export const selectFilteredFavorites=pipe(
    select(selectFavorites),
    filter(val=>val.length!=0)
)