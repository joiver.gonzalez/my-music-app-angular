import { createAction, props, union } from '@ngrx/store';
import { Track } from 'src/app/domain/models/Track/track';



export enum FavoritesActions{
    setFavorites='[Favorites] setFavorites',
    setFavoritesSuccess='[Favorites] setFavoritesSuccess',
    deleteFavorite='[Favorites] deleteFavorite',
    deleteFavoriteSuccess='[Favorites] deleteFavoriteSuccess',
    updateFavorites='[Favorites] updateFavorites',
    updateFavoritesSuccess='[Favorites] updateFavoritesSucces'
}

export const setFavorites=createAction(FavoritesActions.setFavorites);
export const setFavoritesSuccess=createAction(FavoritesActions.setFavoritesSuccess,props<{favorites:Track[]}>());
export const deleteFavorite=createAction(FavoritesActions.deleteFavorite,props<{id:string}>());
export const deleteFavoriteSuccess=createAction(FavoritesActions.deleteFavoriteSuccess,props<{id:string}>());
export const updateFavorites=createAction(FavoritesActions.updateFavorites,props<{newFavorite:Track}>());
export const updateFavoritesSuccess=createAction(FavoritesActions.updateFavoritesSuccess,props<{newFavorite:Track}>());

const all = union({
    setFavorites,
    setFavoritesSuccess,
    deleteFavorite,
    deleteFavoriteSuccess,
    updateFavorites,
    updateFavoritesSuccess
})

export type CoreFavoritesActionsUnion=typeof all;