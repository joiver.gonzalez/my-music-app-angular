import { Action, createReducer, on } from '@ngrx/store';
import { setFavorites, deleteFavorite, updateFavorites, setFavoritesSuccess, deleteFavoriteSuccess, updateFavoritesSuccess } from './favorites.actions'; 
import * as favorites from './favorites.state';

const _favoritesReducer=createReducer(
    favorites.initialState,
    on(setFavorites,(state)=>({...state})),
    on(setFavoritesSuccess,(state,{favorites})=>({...state,favorites:[...favorites]})),
    on(deleteFavorite,(state,{id})=>({...state,id})),
    on(deleteFavoriteSuccess,(state,{id})=>({...state, favorites:[...state.favorites.filter(item=>item.id!==id)]})),
    on(updateFavorites,(state,{newFavorite})=>({...state,newFavorite})),
    on(updateFavoritesSuccess,(state,{newFavorite})=>({...state,favorites:[...state.favorites,newFavorite]})),
)

export function favoritesReducer(state:favorites.State | undefined, action:Action){
    return _favoritesReducer(state,action);
}