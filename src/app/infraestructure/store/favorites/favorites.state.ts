import { Track } from "src/app/domain/models/Track/track";


export interface State{
    favorites:Track[];
}

export const initialState:State={
    favorites:[]
}