import { Injectable, Pipe } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, Observable } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { CoreFavoritesActionsUnion, FavoritesActions, setFavoritesSuccess } from './favorites.actions';
import { TrackApiService } from '../../driven-adapters/track-api/track-api.service';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';

@Injectable()
export class FavoritesEffects{


    loadFavorites$=createEffect(()=>this.actions$.pipe(
        ofType(FavoritesActions.setFavorites),
        mergeMap(
            ()=>this.trackApiService.getByUserId()
            .pipe(
             map(tracks=>({
                type:FavoritesActions.setFavoritesSuccess,
                favorites:tracks
             }))

            )
        )
    ))

    updateFavorites$=createEffect(()=>this.actions$.pipe(
        ofType(FavoritesActions.updateFavorites),
        mergeMap(
            (action)=>this.trackApiService.save(action.newFavorite).pipe(
                map(()=>({
                    type:FavoritesActions.updateFavoritesSuccess,
                    newFavorite:action.newFavorite
                }))
            )
        )
    ))

    delteFavorite$=createEffect(()=>this.actions$.pipe(
        ofType(FavoritesActions.deleteFavorite),
        mergeMap(
            (action)=>this.trackApiService.deleteById(action.id).pipe(
                map(()=>({
                    type:FavoritesActions.deleteFavoriteSuccess,
                    id:action.id
                }))
            )
        )
    ))


    constructor(
        private actions$:Actions<CoreFavoritesActionsUnion>,
        private trackApiService:TrackApiService,
        private store:Store<AppState>
    ){}

}