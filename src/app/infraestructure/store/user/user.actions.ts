import { createAction, props, Action, union } from '@ngrx/store';
import { User } from 'src/app/domain/models/User/user';

export enum UserActions{
    registerUser='[User] registerUser',
    registerUserSucces='[User] registerUserSuccess',
    setUser='[User] setUser',
    setUserSuccess='[User] setUserSuccess',
    unSetUser='[User] unSetUser',
    unSetUserSuccess='[User] unSetUserSuccess'
}

// export class SetUser implements Action{
//     public readonly type = EUserActions.setUser;
//     constructor(public payload: User[]){}
// }
// export class UnSetUser implements Action{
//     public readonly type = EUserActions.unSetUser;
// }
// export type UserActions = SetUser | UnSetUser;

export const setUser=createAction(UserActions.setUser,props<{email:string,password:string}>())

export const setUserSuccess=createAction(UserActions.setUserSuccess,props<{user:User}>());

export const registerUser=createAction(UserActions.registerUser,props<{email:string,password:string}>());

export const registerUserSucces=createAction(UserActions.registerUserSucces,props<{user:User}>())

export const unSetUser=createAction(UserActions.unSetUser);

export const unSetUserSuccess=createAction(UserActions.unSetUserSuccess,props<{user:null}>());


const all = union({
    setUser,
    setUserSuccess,
    registerUser,
    registerUserSucces,
    unSetUser,
    unSetUserSuccess
})

export type CoreUserActionsUnion=typeof all;