import { createSelector } from "@ngrx/store";
import {AppState} from '../app.state';
import * as user from './user.state';

export const _selectUser=(state:AppState)=>state.user;

export const selectUser=createSelector(
    _selectUser,
    (state:user.State)=>state.user
);