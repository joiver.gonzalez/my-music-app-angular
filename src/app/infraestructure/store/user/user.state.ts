import { User } from "src/app/domain/models/User/user";

export interface State {
    user:User| null;
}

export const initialState: State = {
   user:null
}