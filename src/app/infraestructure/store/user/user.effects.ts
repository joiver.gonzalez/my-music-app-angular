import { Injectable } from '@angular/core';
import { Actions, createEffect, CreateEffectMetadata, ofType } from '@ngrx/effects';
import { EMPTY, Observable, from } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { UserApiService } from '../../driven-adapters/user-api/user-api.service';
import { CoreUserActionsUnion, UserActions } from './user.actions';
import { UserMapper } from '../../helpers/maps/user_mapper';
import { User } from 'src/app/domain/models/User/user';


@Injectable()
export class UserEffects{
    mapperUser:UserMapper=new UserMapper();

    loginUser$=createEffect(()=>this.actions$.pipe(
      ofType(UserActions.setUser),
      mergeMap(
        (action)=>this.userApiService.loginUser(action.email,action.password)
      .pipe(
        map(userCredentials=>({
          type:UserActions.setUserSuccess,
          user:this.mapperUser.fromMap(userCredentials)
        }))
      ))
  ))

  registerUser$=createEffect(()=>this.actions$.pipe(
    ofType(UserActions.registerUser),
    mergeMap(
      (action)=>this.userApiService.registerUser(action.email,action.password)
    .pipe(
      map(userCredentials=>({
        type:UserActions.registerUserSucces,
        user:this.mapperUser.fromMap(userCredentials)
      }))
    ))
))

logout$=createEffect(()=>this.actions$.pipe(
  ofType(UserActions.unSetUser),
  mergeMap(
    ()=>this.userApiService.logout()
  .pipe(
    map(()=>({
      type:UserActions.unSetUserSuccess,
      user:null
    }))
  ))
))

    constructor(
        private actions$:Actions<CoreUserActionsUnion>,
        private userApiService:UserApiService
    ){}

   
}

