import { Action, createReducer, on } from '@ngrx/store';
import { setUser, setUserSuccess, unSetUser, registerUser, registerUserSucces } from './user.actions';
import * as user from './user.state';

const _userReducer = createReducer(user.initialState,

    on(setUser,(state,{email,password})=>({...state,email,password})),
    on(setUserSuccess, (state, {user}) => ({ ...state, user: {...user}})),
    on(registerUser,(state,{email,password})=>({...state,email,password})),
    on(registerUserSucces,(state,{user})=>({...state,user:{...user}})),
    on(unSetUser,state=>({...state,user:null})),

);
export function userReducer(state: user.State | undefined, action: Action) {
    return _userReducer(state, action);
}