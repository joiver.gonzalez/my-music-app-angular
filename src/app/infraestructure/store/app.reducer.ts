import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "./app.state";
import {uiReducer} from './ui/ui.reducers';
import {userReducer} from './user/user.reducer';
import {playlistReducer} from './playlist/playlist.reducer';
import { favoritesReducer } from "./favorites/favorites.reducer";
import { tokenReducer } from './token/token.reducer';


export const appReducers:ActionReducerMap<AppState>={
   ui:uiReducer,
   user:userReducer,
   playlist:playlistReducer,
   favorites:favoritesReducer,
   token:tokenReducer
}

