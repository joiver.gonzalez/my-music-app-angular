import { createSelector } from "@ngrx/store";
import {AppState} from '../app.state';
import * as ui from './ui.state';

export const _selectUi=(state:AppState)=>state.ui;

export const selectUi=createSelector(
    _selectUi,
    (state:ui.State)=>state.isLoading
);