import { Action, createReducer, on } from '@ngrx/store';
import { isLoading, stopLoading } from './ui.actions';
import * as ui from './ui.state'


const _uiReducer=createReducer(ui.initialState,
    on(isLoading,state=>({...state,isLoading:true})),
    on(stopLoading,state=>({...state,isLoading:false}))
    )

export function uiReducer(state:ui.State|undefined,action:Action){
    return _uiReducer(state,action);
}