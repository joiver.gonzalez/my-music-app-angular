import {createAction} from '@ngrx/store';

export enum UiActions{
    isLoading='[UI Component] isLoading',
    stopLading='[UI Component] stopLoading'
}

export const isLoading = createAction(UiActions.isLoading);
export const stopLoading = createAction(UiActions.stopLading);