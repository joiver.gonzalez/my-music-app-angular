import { Track } from "src/app/domain/models/Track/track";


export interface State{
    playlist:Track[];
}

export const initialState:State={
    playlist:[]
}