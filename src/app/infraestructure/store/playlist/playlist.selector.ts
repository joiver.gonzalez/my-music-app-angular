import {AppState} from '../app.state';
import * as playlist from './playlist.state';
import { createSelector, select } from '@ngrx/store';
import { pipe, filter } from 'rxjs';


export const _selectPlaylist=(state:AppState)=>state.playlist;

export const selectPlaylist=createSelector(
    _selectPlaylist,
    (state:playlist.State)=>state.playlist
)
export const selectFilteredPlaylist=pipe(
    select(selectPlaylist),
    filter(val=>val.length!=0)
)