import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { PlaylistApiService } from '../../driven-adapters/playlist-api/playlist-api.service';
import { CorePlaylistActionsUnion, PlaylistActions } from './playlist.actions';
import { PlaylistMapper } from '../../helpers/maps/playlist_mapper';
import { of, filter, throwError } from 'rxjs';
@Injectable()
export class PlaylistEffects{

    mapperPlaylist:PlaylistMapper=new PlaylistMapper();

    loadPlaylist$=createEffect(()=>this.actions$.pipe(
        ofType(PlaylistActions.setPlaylist),
        mergeMap(
            (action)=>this.playlistService.getPlaylist(action.token)
        .pipe(
            map(tracks=>({
                type:PlaylistActions.setPLaylistSuccess,
                playlist:this.mapperPlaylist.fromMap(tracks)
            })),
        catchError(()=>{
            localStorage.removeItem("token");
            window.location.reload();
            return throwError(()=>new Error('playlist error token'))
        })   
        )
        )
    ))

    constructor(
        private actions$:Actions<CorePlaylistActionsUnion>,
        private playlistService:PlaylistApiService,
    ){}
}