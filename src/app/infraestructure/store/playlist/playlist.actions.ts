import { createAction, props, union } from '@ngrx/store';
import { Token } from 'src/app/domain/models/Token/token';
import { Track } from 'src/app/domain/models/Track/track';


export enum PlaylistActions{
    setPlaylist='[Playlist] setPlaylist',
    setPLaylistSuccess='[Playlist] setPlaylistSuccess',
    updatePlaylist='[Playlist updatePlaylist]',
    updatePlaylistSuccess='[Playlist] updatePlaylistSuccess'
}

export const setPlaylist=createAction(PlaylistActions.setPlaylist,props<{token:string}>())

export const setPlaylistSuccess=createAction(PlaylistActions.setPLaylistSuccess,props<{playlist:Track[]}>());

export const updatePlaylist=createAction(PlaylistActions.updatePlaylist,props<{isFavorite:boolean, id:string}>());

export const updatePlaylistSuccess=createAction(PlaylistActions.updatePlaylistSuccess,props<{playlist:Track[]}>())

const all = union({
    setPlaylist,
    setPlaylistSuccess, 
    updatePlaylist,
    updatePlaylistSuccess
})

export type CorePlaylistActionsUnion=typeof all;

