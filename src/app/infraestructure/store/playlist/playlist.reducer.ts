import { Action, createReducer, on } from '@ngrx/store';
import { setPlaylist, setPlaylistSuccess, updatePlaylist, updatePlaylistSuccess } from './playlist.actions';
import * as playlist from './playlist.state'
import { Track } from '../../../domain/models/Track/track';

const _playlistReducer=createReducer(
    playlist.initialState,
    on(setPlaylist,(state,{token})=>({...state,token:token})),
    on(setPlaylistSuccess,(state,{playlist})=>({...state,playlist:[...playlist]})),
    on(updatePlaylist,(state,{isFavorite, id})=>(
     {...state,
        playlist:[...state.playlist
            .map((item)=>item.id==id?item=new Track(
                item.id,
                item.name,
                item.artistName,
                item.urlImage,
                isFavorite
            ):item)],
    })),
    on(updatePlaylistSuccess,(state,{playlist})=>({...state,playlist:[...playlist]}))
)

export function playlistReducer(state:playlist.State | undefined, action:Action){
    return _playlistReducer(state,action);
}