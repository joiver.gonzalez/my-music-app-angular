import { UserEffects } from "./user/user.effects";
import { PlaylistEffects } from './playlist/playlist.effects';
import { FavoritesEffects } from "./favorites/favorites.effects";
import { TokenEffects } from './token/token.effects';

export const EffectsArray:any[]=[UserEffects,PlaylistEffects,FavoritesEffects, TokenEffects]