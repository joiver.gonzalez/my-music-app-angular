import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, Router } from '@angular/router';
import { Observable, tap, filter } from 'rxjs';
import { UserApiService } from '../driven-adapters/user-api/user-api.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private userApiService:UserApiService, private router:Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
    return this.userApiService.isAuth().pipe(
      tap(estado=>{
        if(!estado){
          console.log(estado)
          this.router.navigate(['auth/login'])
        }
      })
    ); 
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean{
    return true;  
  }
}
