import { Track } from '../../domain/models/Track/track';

export const detecFavoritesTracksAndReturn=(playlist:Track[],favorites:Track[]):Track[]=>{
    let newTrack: Track;
    let listOfTracksFavorites: Track[] = [];
    playlist.map((tracks) => {
      favorites.map((favs) => {
        if (favs.id == tracks.id) {
          newTrack = favs;
        }
      });
      if (newTrack != null && newTrack.id == tracks.id) {
        listOfTracksFavorites.push(newTrack);
      } else {
        listOfTracksFavorites.push(tracks);
      }
    });
    return listOfTracksFavorites;
}