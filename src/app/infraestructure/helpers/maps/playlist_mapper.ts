import { Mapper } from "./common/maps";
import { Playlist } from "src/app/domain/models/Playlist/playlist";
import { Track } from "src/app/domain/models/Track/track";
import { TrackMapper } from "./track_mapper";

export class PlaylistMapper extends Mapper<Track[]>{
    
    mapper:TrackMapper=new TrackMapper();

    constructor(){
        super();
    }
    fromMap(playlist: Playlist): Track[] {
      return playlist.items.map(item=>this.mapper.fromMap(item.track)
       )
    }

}