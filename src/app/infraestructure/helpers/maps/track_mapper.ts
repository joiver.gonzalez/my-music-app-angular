import { Mapper } from "./common/maps";
import { ReadTrack } from "src/app/domain/models/Playlist/interfaces/playlist.interface";
import { Track } from "src/app/domain/models/Track/track";

export class TrackMapper extends Mapper<Track>{
    
    constructor(){
        super();
    }

    public fromMap(track: ReadTrack): Track {
       return new Track(
        track.id,
        track.name,
        track.artists[0].name,
        track.album.images[1].url,
        false
       )          
    }
  
}