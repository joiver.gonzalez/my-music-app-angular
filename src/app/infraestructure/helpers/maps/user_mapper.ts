import { User } from "src/app/domain/models/User/user";
import { Mapper } from "./common/maps";
import { UserCredential } from '../../../domain/models/User/interfaces/user.auth.interfaces';

export class UserMapper extends Mapper<User>{
   
    constructor(){
        super();
    }

    fromMap(obj: UserCredential): User {
        return new User(
            obj.user?.uid,
            obj.user?.email
        )
    }
}