import { Injectable } from '@angular/core';
import { UserGateway } from 'src/app/domain/models/User/gateway/user-gateway';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable, map, from } from 'rxjs';
import {UserCredential } from 'src/app/domain/models/User/interfaces/user.auth.interfaces';
import { User } from 'src/app/domain/models/User/user';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.state';
import { setUserSuccess, unSetUser } from '../../store/user/user.actions';


@Injectable({
  providedIn: 'root'
})
export class UserApiService extends UserGateway{

  constructor(public auth: AngularFireAuth, private store:Store<AppState>) {
    super();
   }

   initAuthListener(){
    this.auth.authState.subscribe(fUser=>{
      if(fUser?.uid){
        const user=new User(fUser.uid,fUser.email)
        this.store.dispatch(setUserSuccess({user}))
      }else{
        this.store.dispatch(unSetUser())
      }
      
    })
    
  }

  loginUser(email: string, password: string): Observable<UserCredential> {
    return from(this.auth.signInWithEmailAndPassword(email,password))
  }
  registerUser(email: string, password: string): Observable<UserCredential> {
    return from(this.auth.createUserWithEmailAndPassword(email,password));
  }
  logout(): Observable<void> {
    return from(this.auth.signOut());
  }
  isAuth(): Observable<boolean> {
    return this.auth.authState.pipe(
      map(fbUser=>fbUser!=null)
    );
  }

  
   
}
