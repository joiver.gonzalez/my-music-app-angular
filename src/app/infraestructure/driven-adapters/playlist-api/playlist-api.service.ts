import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PlaylistGateway } from '../../../domain/models/Playlist/gateway/playlist-gateway';
import { Observable } from 'rxjs';
import { Playlist } from 'src/app/domain/models/Playlist/playlist';
import { environment } from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class PlaylistApiService extends PlaylistGateway{

  idPlaylist:string='37i9dQZEVXbL1Fl8vdBUba';
  urlSpotify:string=environment.spotifyApi;

  constructor(private http:HttpClient){ 
    super();
  }

  getPlaylist(token: string): Observable<Playlist> {
    const headers=new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })
    return this.http.get<Playlist>(`${this.urlSpotify}/playlists/${this.idPlaylist}/tracks`,{headers})
  }
}
