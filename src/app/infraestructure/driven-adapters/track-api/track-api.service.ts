import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrackGateway } from 'src/app/domain/models/Track/gateway/track-gateway';
import { Observable, map, from, lastValueFrom } from 'rxjs';
import { Track } from 'src/app/domain/models/Track/track';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from 'src/app/domain/models/User/user';
import { Store } from '@ngrx/store';
import { AppState } from './../../store/app.state';
import { selectUi } from '../../store/selectors';


@Injectable({
  providedIn: 'root'
})
export class TrackApiService extends TrackGateway{

  userState!:User | null;
  
  constructor(private firestore: AngularFirestore, private store:Store<AppState>) {
    super();
    this.store.select('user').subscribe(user=>{
      this.userState=user.user
    })
  }

  save(track: Track): Observable<void> {
    const newData={
      id:track.id,
      name:track.name,
      artistName:track.artistName,
      urlImage:track.urlImage,
      isFavorite:true
    }
   return from(this.firestore.doc<Track>(`${this.userState?.uid}/${track.id}`).set(newData));
  }
  getByUserId(): Observable<Track[]> {
    return this.firestore.collection<Track>(`${this.userState?.uid}`).snapshotChanges()
    .pipe(
      map(snapshot=>snapshot.map(doc=>({...doc.payload.doc.data()}))
      )
    )
  }
  deleteById(id: string): Observable<void> {
   return from(this.firestore.doc(`${this.userState?.uid}/${id}`).delete());
  }


}
