import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { TrackApiService } from './track-api.service';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../../../../environments/environment.prod';
import { StoreModule } from '@ngrx/store';
import { appReducers } from '../../store/app.reducer';

describe('TrackApiService', () => {
  let service: TrackApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        StoreModule.forRoot(appReducers)
      ]
    });
    service = TestBed.inject(TrackApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
