import { TestBed, waitForAsync  } from '@angular/core/testing';
import { TokenApiService } from './token-api.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment.prod';
import { Token } from '../../../domain/models/Token/token';
import { of } from 'rxjs';


describe('TokenApiService', () => {

  const expectedUrl=environment.spotifyApiToken;
  const url= "https://accounts.spotify.com/api/token";
  let tokenRes:Token={
    access_token:'ASASASQUIWU6',
    expires_in:3600,
    token_type: 'text' };
  let service: TokenApiService;
  let controller:HttpTestingController;
  

  beforeEach(() => {
    
   
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers:[
      ]
    });
    service = TestBed.inject(TokenApiService);
    controller = TestBed.inject(HttpTestingController);
   
    
  });
  afterEach(()=>{
    controller.verify();
  })
 
  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('Expect the url spotify api token',((done:DoneFn)=>{
    service.getToken().then(token=>{
      console.log(token);
      done()
    });
    const request = controller.expectOne(expectedUrl);
    request.flush(tokenRes);
    expect(request.request.url).toContain(url)
  }));
 
  it('Should return a token',(done:DoneFn)=>{
    service.getToken().then(token=>{
      expect(token).toEqual(tokenRes)
      done()
    });
    const request = controller.expectOne(expectedUrl);
    request.flush(tokenRes);
    //expect(request.request.url).toContain(url)
  });

  it('The request method will be POST',(done:DoneFn)=>{
    service.getToken().then(token=>{
      done()
    });
    const request = controller.expectOne(expectedUrl);
    request.flush(tokenRes);
    expect(request.request.method).toEqual('POST');
  })
 
});
