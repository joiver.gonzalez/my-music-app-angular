import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import { Token } from 'src/app/domain/models/Token/token';
import { TokenGateway } from '../../../domain/models/Token/gateway/token-gateway';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
import { Buffer } from 'buffer';

@Injectable({
  providedIn: 'root'
})
export class TokenApiService extends TokenGateway{

  apiToken:string=environment.spotifyApiToken;
  clientId:string=environment.spotifyApiId;
  clientSecret:string=environment.spotifyClientSecret
  authToken:string=Buffer.from(`${this.clientId}:${this.clientSecret}`, 'utf-8').toString('base64');
  data:string='grant_type=client_credentials';
 

  constructor(private http:HttpClient) {
    super();
   }
   async getToken(): Promise<Token> {
    const headers=new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic '+this.authToken
    })
     const get$=this.http.post<Token>(this.apiToken,this.data,{headers})
     const res1= await lastValueFrom(get$);
     return res1;
   }
}
