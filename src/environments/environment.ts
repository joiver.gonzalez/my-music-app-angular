// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyAjW1ac37XRHxKAJsnvDGgNqJZkg5ihpQM",
  authDomain: "my-music-app-29186.firebaseapp.com",
  projectId: "my-music-app-29186",
  storageBucket: "my-music-app-29186.appspot.com",
  messagingSenderId: "241813382502",
  appId: "1:241813382502:web:07c5b631bcee60b9dc94a7"
  },
  spotifyApi:"https://api.spotify.com/v1",
  spotifyApiToken:"https://accounts.spotify.com/api/token",
  spotifyApiId:"7e8c890a107c4a83aac5153961136181",
  spotifyClientSecret:"a5988b8d206a4da69097e62966b23464"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
